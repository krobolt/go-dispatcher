package example

import (
	"fmt"
	"log"
	"net/http"
	"path/filepath"
	"time"

	"gitlab.com/krobolt/go-dispatcher"
	"gitlab.com/krobolt/go-overlay"
	"gitlab.com/krobolt/go-router"
)

var Template overlay.Overlay

//RenderStatic Render Cached static page render
func RenderStatic(w http.ResponseWriter, r *http.Request, layout string, tag string) {
	err := Template.Render(w, layout, nil)
	if err != nil {
		//todo: show 500 error
		Template.Render(w, "404.html", nil)
	}
}

func home(w http.ResponseWriter, r *http.Request) {
	RenderStatic(w, r, "homepage.html", "010619")
}

//New404 Custom 404 Error Page
func New404(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(404)
	Template.Render(w, "404.html", nil)
}

func main() {

	t, err := overlay.NewTheme("master.html", filepath.Join(filepath.Dir(""), "views"), "layouts")
	if err != nil {
		panic(err)
	}
	Template = t

	dir := "/"
	options := router.NewDefaultOptions()
	names := []string{"name"}

	dispatcher := dispatcher.NewDispatcher(dir, options, names)
	dispatcher.AddHandle(404, New404)
	dispatcher.Add("GET", "/", home, []string{"global"})

	res := dispatcher.Match("GET", "/")

	fmt.Println(res, res.GetHandler())

	srv := &http.Server{
		Addr:         ":8283",
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
	}

	http.Handle("/", dispatcher)

	if err := srv.ListenAndServe(); err != nil {
		log.Println(err)
	}

}
