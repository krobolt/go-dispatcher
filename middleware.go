package dispatcher

import (
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"
)

// Middleware is a wrapped http.HandlerFunc
type Middleware func(http.HandlerFunc) http.HandlerFunc

//LoggingMiddleware log to file
func LoggingMiddleware() Middleware {
	// Create a new Middleware
	return func(f http.HandlerFunc) http.HandlerFunc {
		// Define the http.HandlerFunc
		return func(w http.ResponseWriter, r *http.Request) {
			// Do middleware things
			start := time.Now()
			defer func() { log.Println("DOOOO", time.Since(start)) }()
			// Call the next middleware/handler in chain
			f(w, r)
		}
	}
}

//CacheProvider cache control middleware
func CacheProvider() Middleware {
	return func(f http.HandlerFunc) http.HandlerFunc {
		// Define the http.HandlerFunc
		return func(w http.ResponseWriter, r *http.Request) {
			cacheControl := r.Header.Get("Cache-Control")
			if cacheControl == "" {
				ctype := "type"
				maxAge := "8400"
				cacheControl := fmt.Sprintf(
					"%s, max-age=%s%s",
					ctype,
					maxAge,
					"",
				)
				r.Header.Set("Cache-Control", cacheControl)
			}
			lastMod := r.Header.Get("Last-Modified")
			if lastMod != "" {
				//convert lastMod string to Unix time.
				ifModifiedSince := r.Header.Get("If-Modified-Since")
				if ifModifiedSince != "" {
					if lastMod <= ifModifiedSince {
						w.WriteHeader(http.StatusNotModified)
					}
				}
			}
		}
	}
}

func _etag(key string, days int, w http.ResponseWriter, r *http.Request) bool {
	e := `"` + key + `"`
	w.Header().Set("Etag", e)
	w.Header().Set("Cache-Control", fmt.Sprintf("max-age=%d", 60*60*7*days)) // x days
	if match := r.Header.Get("If-None-Match"); match != "" {
		if strings.Contains(match, e) {
			w.WriteHeader(http.StatusNotModified)
			return true
		}
	}
	return false
}
