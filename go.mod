module gitlab.com/krobolt/go-dispatcher

go 1.13

require (
	github.com/stretchr/testify v1.7.0
	gitlab.com/krobolt/go-overlay v0.0.0-20210222194709-d331ab81e53f
	gitlab.com/krobolt/go-router v0.0.0-20210810232225-5d023a4b75e8
)
