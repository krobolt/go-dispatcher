package dispatcher

import (
	"bytes"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

// SimpleHandle Handler Example.
func SimpleHandle(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "/output")
}

func Test_NewHandle(t *testing.T) {
	ident := "id"
	names := []string{"name"}
	expected := &handler{
		id:     ident,
		handle: SimpleHandle,
		name:   names,
	}
	actual := NewHandle(ident, SimpleHandle, names)
	assert.IsType(t, expected, actual)
}

func Test_GetHandler(t *testing.T) {

	ident := "id"
	names := []string{"name"}

	expected := &handler{
		id:     ident,
		handle: SimpleHandle,
		name:   names,
	}

	actual := NewHandle(ident, SimpleHandle, names)
	ahandler := actual.GetHandler()
	ehandler := expected.GetHandler()

	req, rsp := NewMockRequestResponse()
	ahandler(rsp, req)
	ehandler(rsp, req)

	actualOutput := rsp.Body.String()
	expectedOutput := "/output/output"
	assert.Equal(t, expectedOutput, actualOutput)
}

func Test_GetID(t *testing.T) {
	h := NewMockHandle()
	expected := "mock-id"
	actual := h.GetID()
	assert.Equal(t, expected, actual)
}

func Test_GetNames(t *testing.T) {
	h := NewMockHandle()
	expected := []string{"name"}
	actual := h.GetNames()
	assert.Equal(t, expected, actual)
}

func Test_GetHandlerFromHandleFunc(t *testing.T) {

}

/**
* MOCK OBJECTS
 */
func NewMockHandle() Handler {
	return &handler{
		id: "mock-id",
		handle: func(w http.ResponseWriter, r *http.Request) {
			fmt.Fprintf(w, ".")
		},
		name: []string{"name"},
	}
}

func NewMockRequestResponse() (*http.Request, *httptest.ResponseRecorder) {
	req, _ := http.NewRequest("GET", "http://localhost:8080/", nil)
	rsp := NewRecorder()
	return req, rsp
}

// NewRecorder returns an initialized ResponseRecorder.
func NewRecorder() *httptest.ResponseRecorder {
	return &httptest.ResponseRecorder{
		HeaderMap: make(http.Header),
		Body:      new(bytes.Buffer),
	}
}
