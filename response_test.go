package dispatcher

import (
	"fmt"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/krobolt/go-router"
)

func Test_NewResponse(t *testing.T) {

	r := CreateMockRouterResponse()
	h := CreateMockHandle()
	c := 200

	expected := CreateMockResponse(r, h, c)
	actual := NewResponse(r, h, c)
	assert.Equal(t, expected, actual)
}

func Test_GetName(t *testing.T) {
	res := CreateMockResponse(CreateMockRouterResponse(), CreateMockHandle(), 200)
	expected := []string{"name"}
	actual := res.GetNames()
	assert.Equal(t, expected, actual)
}
func Test_GetVars(t *testing.T) {
	r := CreateMockRouterResponse()
	h := CreateMockHandle()
	res := CreateMockResponse(r, h, 200)
	expected := r.GetVars()
	actual := res.GetVars()
	assert.Equal(t, expected, actual)
}

func Test_GetRouterResponse(t *testing.T) {
	r := CreateMockRouterResponse()
	h := CreateMockHandle()
	c := 200
	res := CreateMockResponse(r, h, c)
	expected := r
	actual := res.GetRouterResponse()
	assert.Equal(t, expected, actual)
}
func Test_ResponseGetHandler(t *testing.T) {
	r := CreateMockRouterResponse()
	h := CreateMockHandle()
	c := 200
	res := CreateMockResponse(r, h, c)
	expected := h
	actual := res.GetHandler()
	assert.Equal(t, expected, actual)
}
func Test_GetStatusCode(t *testing.T) {
	r := CreateMockRouterResponse()
	h := CreateMockHandle()
	expected := 200
	res := CreateMockResponse(r, h, expected)
	actual := res.GetStatusCode()
	assert.Equal(t, expected, actual)
}

func MockHandle(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "response")
}

func CreateMockHandle() Handler {
	return &handler{
		id:     "id",
		handle: MockHandle,
		name:   []string{"name"},
	}
}

func CreateMockRouterResponse() router.Response {
	vars := make(map[string]interface{})
	vars["text"] = "value"
	vars["int"] = 100
	return router.NewResponse(
		2,
		router.NewRoute(
			"GET",
			"/",
			make([]router.RouteParam, 0),
			make([]router.RouteAlias, 0),
			"id",
		),
		vars,
	)
}

func CreateMockResponse(r router.Response, h Handler, c int) Response {
	return &dispatchResponse{
		res:     r,
		handler: h,
		code:    c,
	}
}
