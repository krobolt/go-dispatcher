
# Go-Dispatcher

## Install 

go get gitlab.com/krobolt/go-dispatcher

Dispatcher adds named routes, http handlers and middlware to go-router.


## HTTP Handlers

```
	func (w http.ResponseWriter, r *http.Request)
```

## Middleware


```
	func() dispatcher.Middleware
```

Middleware can be attached to named routes, (FIFO) which can executed before and after the handler
using the following pattern:

```
	example := func() dispatcher.Middleware {
		return func(f http.HandlerFunc) http.HandlerFunc {
			return func(w http.ResponseWriter, r *http.Request) {

				//Code to be executed before handler

				//Execute handler
				f(w, r)

				//Code be to executed after handler
			}
		}
	}
```

See gitlab.com/krobolt/go-router for defining routes/route patterns

Example
Attach new route with "global" namespacec and "homepage" attached to index

```
	dir := "/"
	options := router.NewDefaultOptions()
	names := []string{"global"}

	dispatcher := dispatcher.NewDispatcher(dir, options, names)
	dispatcher.Add("GET", "/", home, []string{"homepage"})
```

Attach middleware to "homepage"

```
	dispatcher.Set("homepage", example)
```