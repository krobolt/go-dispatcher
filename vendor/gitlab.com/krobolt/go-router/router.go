package router

import (
	"errors"
	"regexp"
	"strings"
)

//Router interface
type Router interface {
	//Group routes together under a common directory/name
	Group(path string, callback func())
	//Add Route to router
	Add(method string, path string) (Route, error)
	//Match route and return the id as string, status as int
	//Status Codes:
	//0 - No Match
	//1 - Match but wrong method
	//2 - Match with correct method
	Find(method string, path string) (string, int)
	//Match route and return a router.Respons
	Match(method string, path string) Response
	//WithOptions updates router settings after initialisation
	GetOptions() Options
	//GetBase returns base path
	GetBase() string
}

//Options for changing how to parsing provided paths
// : + `\(([` + `a-z\-_AZ0-9` + `+]+)\)` + varOpen + `[` + varLoopuk + `]+` + varClose
//`:` + `\(([` + `a-zAZ0-9.\-_` + `+]+)\)` + `-(` + `[` + `a-zAZ0-9.\-_` + `]+` + `)`

type Options struct {
	VarSep          string //     	`:`
	OptionalSep     string //  	 	`|`
	AllowedChars    string // 		`a-z\-_AZ0-9`
	AllowedVarChars string //		`a-z\-_AZ0-9`
	Separator       string //   	 "/"
	// : + `\(([` + `a-z\-_AZ0-9` + `+]+)\)` + varOpen + `[` + varLoopuk + `]+` + varClose
	//`:` + `\(([` + `a-zAZ0-9.\-_` + `+]+)\)` + `-(` + `[` + `a-zAZ0-9.\-_` + `]+` + `)`
	Pattern string
}

type router struct {
	o        Options
	routes   map[string]Route
	loopup   map[int][]RouteAlias
	basepath string
}

//NewRouter to create router with default settings. Router only aceppts method and path and returns a route with a unique id @see dispatcher
func NewRouter(path string, options Options) *router {
	return &router{
		o:        options,
		routes:   make(map[string]Route),
		loopup:   make(map[int][]RouteAlias),
		basepath: path,
	}
}

//NewDefaultOptions creates router with common settings
func NewDefaultOptions() Options {
	vs := `:`
	open := `-(`
	close := `)`
	allowedChars := `a-zAZ0-9.\-_`
	allowedVarChars := `a-zAZ0-9.\-_`
	return Options{
		VarSep:          vs,
		OptionalSep:     `|`,
		AllowedChars:    allowedChars,
		AllowedVarChars: allowedVarChars,
		Pattern:         vs + `\(([` + allowedChars + `+]+)\)` + open + `[` + allowedVarChars + `]+` + close,
		Separator:       "/",
	}
}

func (r *router) GetBase() string {
	return r.basepath
}

func (r *router) Group(path string, callback func()) {
	parent := r.basepath
	r.basepath = parent + path
	callback()
	r.basepath = parent
}

func (r *router) Add(method string, path string) (Route, error) {

	p := r.o.Separator + strings.Trim(r.basepath, r.o.Separator) + strings.Trim(path, r.o.Separator)

	if p == r.o.Separator {

		i, _ := CreateID(method, path)
		re := NewRoute(method, path, nil, nil, i)
		r.routes[re.GetID()] = re
		a := NewRouteAlias(i, r.o.Separator, 0)
		r.loopup[0] = append(r.loopup[0], a)
		return re, nil
	}

	rt, err := r._newRoute(method, p)

	if err != nil {
		return rt, err
	}

	if r.routes[rt.GetID()] != nil {
		return rt, errors.New("route has already been added" + rt.GetPath())
	}

	r.routes[rt.GetID()] = rt

	for _, a := range rt.GetAliases() {

		i := a.GetArgCount()

		if len(r.loopup[i]) < 1 {
			r.loopup[i] = make([]RouteAlias, 0)
		}

		r.loopup[i] = append(r.loopup[i], a)
	}

	return rt, err
}

//Match method and route to added routes and return route ID
//returns the id of the route
//returns 0 - no match, 1 - path match but wrong method, 2 - method and path match
func (r *router) Find(method string, path string) (string, int) {
	args := strings.Split(strings.TrimRight(path, r.o.Separator), r.o.Separator)
	for _, v := range r.loopup[len(args)-1] {
		if _matchRegex(v.GetURI(), path) {
			if r.routes[v.GetID()].GetMethod() == method {
				return v.GetID(), 2
			}
			return "", 1
		}
	}
	return "", 0
}

//Match method and path to added routes and return Route instance.
func (r *router) Match(method string, path string) Response {

	path = strings.TrimRight(path, r.o.Separator)
	splituri := strings.Split(path, r.o.Separator)
	uriLen := len(splituri) - 1

	if path == "" {
		path = "/"
	}

	er := NewRoute(method, path, nil, nil, "")
	results := make(map[string]interface{})

	valid := false

	for _, v := range r.loopup[uriLen] {

		if _matchRegex(v.GetURI(), path) {

			if r.routes[v.GetID()].GetMethod() == method {

				found := r.routes[v.GetID()]
				params := found.GetParams()

				for _, p := range params {

					if p.GetName() != "" {
						res := splituri[p.GetIndex()]
						rar := strings.Split(res, p.PartialURI())
						resout := strings.Join(rar, "")
						results[p.GetName()] = resout
					}
				}
				return NewResponse(2, r.routes[v.GetID()], results)
			}
			valid = true
		}
	}

	if valid {
		//found but wrong method
		return NewResponse(1, er, nil)
	}

	//no route matched.
	return NewResponse(0, er, nil)

}

func (r *router) _newRoute(method string, path string) (Route, error) {

	i, err := CreateID(method, path)
	re := NewRoute("", path, nil, nil, i)

	if err != nil {
		return re, err
	}

	//p, err := GetParamsFromPath(r.o.OptionalSep, r.o.VarSearch, r.o.Seperator, path)
	p, err := r._getParamsFromPath(path)
	if err != nil {
		return re, err
	}

	//return NewRoute(method, path, p, r._createRouteAliases(i, p), i), err
	return NewRoute(method, path, p, r._createRouteAliases(i, p), i), err
}

func (r *router) _createRouteAliases(ident string, p []RouteParam) []RouteAlias {

	var (
		alias []RouteAlias // collection of route alias's
		abuf  []string     // optional param's to to appended to base url
		buf   []string     // string buffer for base url and named routed.
		regex string
		path  string
	)

	alias = make([]RouteAlias, 0)
	abuf = make([]string, 0)
	buf = make([]string, 2)

	buf[0] = r.o.Separator
	buf[1] = buf[0]
	for _, v := range p {
		regex = v.GetRegex()
		path = v.GetPath()
		switch v.GetGroup() {
		case r.o.OptionalSep:
			abuf = append(abuf, v.GetRegex()+r.o.Separator)
		default:
			buf[0] = buf[0] + regex + r.o.Separator
			buf[1] = buf[1] + path + r.o.Separator
		}
	}
	buf[1] = strings.TrimRight(buf[1], r.o.Separator)
	alias = append(alias, NewRouteAlias(ident, strings.TrimRight(buf[0], r.o.Separator), len(strings.Split(buf[1], r.o.Separator))-1))
	buf[1] = ""
	for i := 0; i < len(abuf); i++ {
		buf[1] = buf[1] + abuf[i]
		alias = append(alias, NewRouteAlias(
			ident,
			strings.TrimRight(buf[0]+buf[1], r.o.Separator),
			alias[0].GetArgCount()+(i+1)),
		)
	}
	return alias
}

func (r *router) GetOptions() Options {
	return r.o
}

func (r *router) _getParamsFromPath(path string) ([]RouteParam, error) {

	baseUri := ""
	buf := make([]string, 1)
	params := make([]RouteParam, 0)

	//get optional vars
	//process as additional
	_, o := _split(r.o.OptionalSep, path)

	//split and get param count for non optional parts
	para := strings.Split(o[0], r.o.Separator)

	//for each param
	findBaseUri := true
	for c := 0; c < len(para); c++ {

		//split variable
		v := strings.Split(para[c], r.o.VarSep)
		if len(v) == 1 {
			if findBaseUri && para[c] != "" {
				baseUri = baseUri + "/" + para[c]
			}
			continue
		}
		findBaseUri = false
		buf[0] = baseUri

		buf = append(buf, v[0]+":"+v[1])

	}

	index := r._getArgCount(baseUri)

	for _, v := range buf[1:] {
		index++
		v = r._trimnPath(v)
		p, e := CreateParamFromString(r.o.Pattern, r.o.Separator, "req", v, index)
		if e != nil {
			return params, e
		}
		params = append(params, p)
	}

	for _, v := range o[1:] {
		index++
		v = strings.Trim(v, r.o.Separator)
		if len(strings.Split(v, r.o.Separator)) > 1 {
			return params, errors.New("wnable to add route, required fields cannot follow optional one")
		}
		p, _ := CreateParamFromString(r.o.Pattern, r.o.Separator, "opt", v, index)
		params = append(params, p)
	}

	p, e := CreateParamFromString(r.o.Pattern, r.o.Separator, "base", strings.Trim(baseUri, r.o.Separator), 0)

	if e != nil {
		return params, e
	}

	params = _prependParam(p, params)

	if !_isValid(path, params) {
		return params, errors.New("route does not compile back to prodivded path")
	}

	return params, e
}

func (r *router) _trimnPath(s string) string {
	return strings.Trim(s, r.o.Separator)
}

func (r *router) _getArgCount(s string) int {
	return len(strings.Split(r._trimnPath(s), r.o.Separator))
}

//_matchRegex Match provided pattern, p, to string, s.
func _matchRegex(p string, s string) bool {
	if len(p) > 1 && len(s) > 1 {
		if p[:2] != s[:2] && p[:2] != "/[" {
			return false
		}
	}
	m := regexp.MustCompile(p)
	r := m.FindStringSubmatch(s)

	if len(r) > 0 {
		return r[0] == s
	}
	return false
}

func _isValid(path string, params []RouteParam) bool {
	actual := ""
	for _, rp := range params {
		actual = actual + "/" + rp.GetSource()
	}
	return path == actual
}

func _prependParam(p RouteParam, route []RouteParam) []RouteParam {
	route = append(make([]RouteParam, 1), route...)
	route[0] = p
	return route
}

func _split(i string, s string) (bool, []string) {
	s = strings.Trim(s, i)
	op := strings.Split(s, i)
	if len(op) <= 1 {
		return false, []string{s}
	}
	return true, op
}

func _newErrorParam(e string) (RouteParam, error) {
	return NewParam("error", "", "", "", "", 0), errors.New(e)
}
