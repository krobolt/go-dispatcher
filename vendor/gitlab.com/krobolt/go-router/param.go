package router

//RouteParam is the basic parts of the path provided
type RouteParam interface {
	GetName() string
	GetGroup() string
	GetRegex() string
	GetPath() string
	GetSource() string
	GetIndex() int
	SetPartional(s string)
	PartialURI() string
}

// param is the representation of a compiled url param as a regular expression.
type param struct {
	g       string
	s       string
	r       string
	p       string
	n       string
	i       int
	Partial string
}

//GetName returns RouteParam name
func (p *param) GetName() string {
	return p.n
}

//GetGroup returns RouteParam type group
//Names are "base","optional","required"
func (p *param) GetGroup() string {
	return p.g
}

//GetRegex returns RouteParam regex string
func (p *param) GetRegex() string {
	return p.r
}

//GetPath returns RouteParam path
func (p *param) GetPath() string {
	return p.p
}

//GetSource returns original path provided to create RouteParam
func (p *param) GetSource() string {
	return p.s
}

//GetSource returns original path provided to create RouteParam
func (p *param) GetIndex() int {
	return p.i
}

func (p *param) SetPartional(s string) {
	p.Partial = s
}

func (p *param) PartialURI() string {
	return p.Partial
}

// NewParam returns instance of RouteParam, router.param
// group: group name e.g base, optional, required
// souce: original input e.g. (a-z)-varname
// regex: replacement pattern e.g. [a-z]+
// path: path name e.g. varname
// name: name of variable if set e.g varname
func NewParam(group string, source string, regex string, path string, name string, index int) RouteParam {
	return &param{
		g:       group,
		s:       source,
		r:       regex,
		p:       path,
		n:       name,
		i:       index,
		Partial: "",
	}
}
