// Package router implements regex based route matching.
/*go
The Router itself does nothing but breaks down a method/path into a series of route alias's which all share the same ID.

Matches can either return the ID of the route or return a router.

Response that contains status code information and resolved variables for that path.

Creating the Router

	router := router.NewRouter("/baseuri")

Adding Routes

	router.Add("method", "/path")

Grouping Routes

	router.Group("/group-path",func(){
		router.Add("method","/path")
		router.Add("another-method","/another-path")
	})

Adding Required Fields

Required fields are started with router.RouterOptions.VarSep, default ':'

Allowed chars are then specified within brackets:

	`(a-z)` = lowercase `a` to `z` cast variable as string
	`(A-Z)` = uppercase `A` to `Z` cast variable as string
	`(0-9)` = int only 0,1,2,..9 case variable to int64
	`(a-zA-z0-9-.)` = allow 0-9, upper and lower case. Allow `.`, `-` case as string

The name for that variable is then followed by the hypen `-VARNAME`

Variable name can be uppper/lowercase/numbers and contain `.`,`_`,`-` this can be changed by updating router.RouterOptions.AllowedVars

	router.Add("GET", "/some/path/:(a-z)-variable")

Would match:

	/some/path/lowercaseword
	map["variable"] = "lowercaseword"

Examples

	router.Add("GET", "/blog/post/:(0-9)-year|/:(a-z\-)-title")

	/blog/post/2018
	/blog/post/2018/my-blog-title
	/blog/post/2018/another-blog-title
	/blog/post/2018/another-blog-title-with-int-898222

Prefixing/Suffixing Variable Routes

	use '!` to denote
	router.Add("GET", "/blog/post/year-!:(0-9)-year)

	would match:
	/blog/post/year-1999
	with var ['year'] = 1999

	router.Add("GET", "/blog/post/:(0-9)-year!-year)

	would match:
	/blog/post/1999-year
	with var ['year'] = 1999





Matching Routes

	router.Response = r.Match(method, post)

	router.Response.GetStatusCode()
	router.Response.GetVars()
	router.Response.GetRoute()
	router.Response.GetRoute().GetID()


Customising the router

	options := router.NewDefaultOptions()

	options.VarSep:       `:`,
	options.OptionalSep:  `&`,
	options.DefaultInput:  `1-9`,
	options.AllowedVars:   `a-z`,
	options.VarSearch:    options.VarSep + `\(([` + options.DefaultInput + `+]+)\)` + `-(` + `[` + options.AllowedVars + `]+` +  `)`,
	options.Seperator:    `0`,

*/
package router
