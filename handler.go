package dispatcher

import (
	"net/http"
)

type Handler interface {
	GetHandler() http.HandlerFunc
	GetNames() []string
	GetID() string
}

type handler struct {
	id     string
	handle http.HandlerFunc
	name   []string
}

//NewHandle create a new route handler
func NewHandle(ident string, HandlerFunc http.HandlerFunc, names []string) Handler {
	return &handler{
		id:     ident,
		handle: HandlerFunc,
		name:   names,
	}
}

func (h *handler) GetID() string {
	return h.id
}

func (h *handler) GetHandler() http.HandlerFunc {
	return h.handle
}

func (h *handler) GetNames() []string {
	return h.name
}

// GetHandlerFromHandleFunc wraps a http.HandlerFunc (SimpleHandle) and returns a http.Handler
// for example SimpleHandle can be passed to GetHandlerFromHandleFunc to be used in GenerateMiddlewareChain
// to create a http.HandlerFunc collection of SimpleHandle and Middlewares
func GetHandlerFromHandleFunc(h http.HandlerFunc) http.Handler {
	return http.HandlerFunc(h)
}

//GenerateMiddlewareChain wraps http.HandlerFunc with middleware collection and returns http.HandlerFunc
func GenerateMiddlewareChain(f http.HandlerFunc, middlewares ...Middleware) http.HandlerFunc {
	for _, m := range middlewares {
		f = m(f)
	}
	return f
}
