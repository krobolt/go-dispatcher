package dispatcher

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"time"

	router "gitlab.com/krobolt/go-router"
)

//Dispatcher is responsible for linking a route id to an http.Handlerfunc
type Dispatcher interface {
	ServeHTTP(w http.ResponseWriter, r *http.Request)
	Group(path string, callback func(), name []string)
	Add(method string, path string, handle http.HandlerFunc, names []string) error
	Match(method string, path string) Response
	GetMiddleware(name string) (func() Middleware, error)
	GetRouteMiddlewares(names []string) ([]Middleware, error)
	SetMiddleware(name string, m func() Middleware) error
	AddHandle(code int, h http.HandlerFunc)
}

type dispatcher struct {
	r        router.Router
	handles  map[string]Handler
	name     []string
	m        map[string]func() Middleware
	path     string
	handlers map[int]http.HandlerFunc
}

//NewDispatcher returns new Dispatcher
func NewDispatcher(dir string, options router.Options, names []string) Dispatcher {
	return &dispatcher{
		r:        router.NewRouter(dir, options),
		handles:  make(map[string]Handler),
		m:        make(map[string]func() Middleware),
		name:     names,
		path:     dir,
		handlers: make(map[int]http.HandlerFunc),
	}
}

func (d *dispatcher) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	method := r.Method
	path := r.URL.String()
	response := d.Match(method, path)
	handler := response.GetHandler()
	handle := handler.GetHandler()
	vars := response.GetVars()
	r.URL.RawQuery = BuildQueryFromVars(vars)
	//respond
	handle(w, r)
}

//BuildQueryFromVars
func BuildQueryFromVars(vars map[string]interface{}) string {
	rawQuery := ""
	for k, v := range vars {
		fmt.Println(k, v, "::")
		rawQuery = rawQuery + k + "=" + v.(string) + "&"
	}
	rawQuery = strings.TrimRight(rawQuery, "&")
	return rawQuery
}

func (d *dispatcher) GetDefaultErrorHandler(code int) http.HandlerFunc {
	if len(d.handlers) > 0 && d.handlers[code] != nil {
		return d.handlers[code]
	}
	switch code {
	case 404:
		return defaultNotFoundHandler
	case 405:
		return defaultNotAllowedHandler
	default:
		return defaultServerErrorHandler
	}
}

func defaultNotFoundHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Default 404 Page Not Found Handler")
}
func defaultNotAllowedHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Default 405 Method Not Allowed Handler")
}
func defaultServerErrorHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Default 500 Internal Server Error Handler")
}

func (d *dispatcher) AddHandle(code int, h http.HandlerFunc) {
	d.handlers[code] = h
}

func (d *dispatcher) SetMiddleware(name string, m func() Middleware) error {
	if d.m[name] != nil {
		return errors.New("middleware already set. Unable to set middleware for named route: " + name)
	}
	d.m[name] = m
	return nil
}

//GetMiddleware returns middleware if found
func (d *dispatcher) GetMiddleware(name string) (func() Middleware, error) {
	ms := d.m[name]
	if ms != nil {
		return ms, nil
	}
	return ms, errors.New("unable to locate middlware: " + name)
}

//GetMiddleware returns middlewares for provided route names
func (d *dispatcher) GetRouteMiddlewares(names []string) ([]Middleware, error) {
	out := []Middleware{}
	for _, name := range names {
		ms, err := d.GetMiddleware(name)
		if err != nil {
		} else {
			out = append(out, []Middleware{ms()}...)
		}

	}

	return out, nil
}

//Group Add named group of paths.
func (d *dispatcher) Group(path string, callback func(), name []string) {

	globalNames := d.name
	opt := d.r.GetOptions()

	newGlobalName := append(globalNames, name...)
	d.name = newGlobalName

	basePath := opt.Separator + strings.Trim(d.path, opt.Separator) // "/" , "/base" ...
	groupBase := path                                               // /group

	if basePath != "/" {
		groupBase = basePath + path // /group
	}

	d.path = groupBase //new path = /base
	callback()
	d.path = d.r.GetBase()
	d.name = newGlobalName
}

func (d *dispatcher) Add(method string, path string, handle http.HandlerFunc, names []string) error {

	for _, g := range d.name {
		if !_sliceContainsString(names, g) {
			names = append(names, g)
		}
	}

	r, err := d.r.Add(method, d.path+path) //add route to the router and return route.
	if err != nil {
		return err
	}

	id := r.GetID()
	err = _addHandle(d, id, NewHandle(id, handle, names))
	return err
}

//Match method, path and return a Handler
func (d *dispatcher) Match(method string, path string) Response {

	response := d.r.Match(method, path)
	id := "error404"
	httpStatus := 404
	handle := d.GetDefaultErrorHandler(httpStatus)         // get http.Handlerfunc
	handler := NewHandle(id, handle, []string{"error404"}) //create Handler
	switch response.GetStatusCode() {
	case 2:
		httpStatus = 200
		handler = d.handles[response.GetRoute().GetID()]
	case 1:
		//if the requested method is GET then return 404
		if method != "GET" {
			httpStatus = 405
			id = "error405"
			handle = d.GetDefaultErrorHandler(httpStatus)         // get http.Handlerfunc
			handler = NewHandle(id, handle, []string{"error405"}) //create Handler
		}
	}
	return NewResponse(response, handler, httpStatus)
}

//check middleware collection for name
func _sliceContainsString(slice []string, value string) bool {
	for _, v := range slice {
		if v == value {
			return true
		}
	}
	return false
}

func _addHandle(d *dispatcher, id string, h Handler) error {

	baseHandle := h.GetHandler()
	m, _ := d.GetRouteMiddlewares(h.GetNames())
	chained := GenerateMiddlewareChain(baseHandle, m...)
	handler := NewHandle(h.GetID(), chained, h.GetNames())

	if d.handles[id] != nil {
		return errors.New("failed to add Handler, handler id already added: " + id)
	}
	d.handles[id] = handler
	return nil
}

//"cert.pem", "key.pem", nil

//Serve Application
func Serve(port string, h http.Handler, logger *log.Logger) {

	fmt.Println("starting server")

	var wait time.Duration
	flag.DurationVar(
		&wait,
		"graceful-timeout",
		time.Second*15,
		"the duration for which the server gracefully wait for existing connections to finish - e.g. 15s or 1m",
	)

	srv := &http.Server{
		Addr:         port,
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		//Handler:      h,
		ErrorLog: logger,
	}

	fs := http.FileServer(http.Dir("static"))
	http.Handle("/static/", http.StripPrefix("/static/", fs))
	http.Handle("/", h)

	go func() {
		if err := srv.ListenAndServe(); err != nil {
			log.Println(err)
		}

	}()

	c := make(chan os.Signal, 1)
	// Graceful shutdowns when quit via SIGINT (Ctrl+C)
	signal.Notify(c, os.Interrupt)
	// Block until we receive our signal.
	<-c
	// Create a deadline to wait for.
	ctx, cancel := context.WithTimeout(context.Background(), wait)
	defer cancel()
	// Doesn't block if no connections, but will otherwise wait
	// until the timeout deadline.
	srv.Shutdown(ctx)
	// Optionally, you could run srv.Shutdown in a goroutine and block on
	// <-ctx.Done() if your application should wait for other services
	// to finalize based on context cancellation.
	log.Println("shutting down")
	os.Exit(0)
}
func ServeTLS(port string, h http.Handler, logger *log.Logger, cert string, key string) {
	var wait time.Duration
	flag.DurationVar(
		&wait,
		"graceful-timeout",
		time.Second*15,
		"the duration for which the server gracefully wait for existing connections to finish - e.g. 15s or 1m",
	)

	srv := &http.Server{
		Addr:         port,
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		//Handler:      h,
		ErrorLog: logger,
	}

	fs := http.FileServer(http.Dir("static"))
	http.Handle("/static/", http.StripPrefix("/static/", fs))
	http.Handle("/", h)

	go func() {
		if err := srv.ListenAndServeTLS(cert, key); err != nil {
			panic(err)
		}
	}()

	c := make(chan os.Signal, 1)
	// Graceful shutdowns when quit via SIGINT (Ctrl+C)
	signal.Notify(c, os.Interrupt)
	// Block until we receive our signal.
	<-c
	// Create a deadline to wait for.
	ctx, cancel := context.WithTimeout(context.Background(), wait)
	defer cancel()
	// Doesn't block if no connections, but will otherwise wait
	// until the timeout deadline.
	srv.Shutdown(ctx)
	// Optionally, you could run srv.Shutdown in a goroutine and block on
	// <-ctx.Done() if your application should wait for other services
	// to finalize based on context cancellation.
	log.Println("shutting down")
	os.Exit(0)
}

/**
Cache and HTTP2.0 Push
**/
func ETag(key string, days int, w http.ResponseWriter, r *http.Request) bool {
	e := `"` + key + `"`
	w.Header().Set("Etag", e)
	w.Header().Set("Cache-Control", fmt.Sprintf("max-age=%d", 60*60*7*days)) // x days
	if match := r.Header.Get("If-None-Match"); match != "" {
		fmt.Println("match", match)
		if strings.Contains(match, e) {
			w.WriteHeader(http.StatusNotModified)
			return true
		}
	}
	return false
}
